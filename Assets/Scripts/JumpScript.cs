﻿using UnityEngine;
using System.Collections;

public class JumpScript : MonoBehaviour {
	public GameObject center;
	public GameObject sprite;
	[HideInInspector]
	public bool inAir;
	[Range(0.0f, 2.0f)]
	public float jumpStr=1;
	


	void OnCollisionEnter2D(Collision2D coll) {
	
		inAir = false;

	}
	void OnCollisionExit2D(Collision2D coll) {

		inAir = true;
	}
	void Start () {
		
	}
	

	void Update () {
				

				
				if (inAir) {
			sprite.transform.localScale = new Vector3 (1f- Mathf.Abs (transform.position.y - center.transform.position.y) * jumpStr,1f+ Mathf.Abs (transform.position.y - center.transform.position.y)*jumpStr, 1);
			//sprite.transform.localScale = new Vector3 (1 - Mathf.Abs (rigidbody2D.velocity.y / 20), 1 + Mathf.Abs (rigidbody2D.velocity.y / 20), 1);
				} else {
			sprite.transform.localScale = new Vector3 (0.9f+ Mathf.Abs (transform.position.y - center.transform.position.y) * 2*jumpStr,1.1f- Mathf.Abs (transform.position.y - center.transform.position.y) * 2*jumpStr, 1);
				}
		}

}
