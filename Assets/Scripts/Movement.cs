﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
		public bool allowJump = false;
		public bool allowDash = false;
	public bool allowSlideJump = false;
	public bool jumped;
		public float jumpStr;
		public int linearVelocity = -8;
	public int slidingVelocity =1;
		public int velocityDirection = 1;
		public GameObject sprite;

		void OnCollisionEnter2D (Collision2D coll)
		{
		jumped=false;
				if (coll.gameObject.tag == "Wall") {
						allowDash = true;

						velocityDirection *= -1;
			rigidbody2D.angularDrag=2;
				}
				if (coll.gameObject.tag == "Floor")
						allowJump = true;
				if (coll.gameObject.tag == "Slider") {
						transform.rotation = coll.transform.rotation;
			allowSlideJump=true;
				}

		}

		void OnCollisionStay2D (Collision2D coll)
		{
				if (coll.gameObject.tag == "Floor") {

						rigidbody2D.velocity = new Vector2 (linearVelocity * velocityDirection, rigidbody2D.velocity.y);
				}
				if (coll.gameObject.tag == "Slider") {
			
			rigidbody2D.velocity = transform.right*slidingVelocity;
				}
				if (coll.gameObject.tag == "Wall" && (Input.touchCount > 0 || Input.GetKey (KeyCode.Space)) && allowDash) {
						
								Dash ();
						
				}
				if (((Input.touchCount > 0) || Input.GetKey (KeyCode.Space)) && allowJump && coll.gameObject.tag == "Floor") {
			
			
						Jump ();
				}
		if (((Input.touchCount > 0) || Input.GetKey (KeyCode.Space)) && allowSlideJump && coll.gameObject.tag == "Slider") {
			
			
			Jump();
		}
		}

		void OnCollisionExit2D (Collision2D coll)
		{
				if (coll.gameObject.tag == "floor")
						allowJump = false;
				if (coll.gameObject.tag == "wall") {
						allowDash = false;
			rigidbody2D.angularDrag=0;
				}
				if (coll.gameObject.tag == "Slider")
						transform.rotation = new Quaternion (0, 0, 0,0);
		}
	
		void Start ()
		{

	
		}

		public void Jump ()
		{
				allowJump = false;
				rigidbody2D.AddForce (Vector2.up * jumpStr);
		jumped = true;
			
		}


		public void Dash ()
		{
				rigidbody2D.velocity = new Vector2 (0, 0);

				rigidbody2D.AddForce (new Vector2 (600 * velocityDirection,1000));
				allowDash = false;
		jumped = true;
		
		}

		void Update ()
		{
				if (sprite.transform.localScale.x > 1.19) {
						jumpStr = 1200;
				} else {
						jumpStr = 700;
				}
		if(jumped) rigidbody2D.AddForce (Vector2.right*5*velocityDirection);
				

	
			//duperele


		}
}
