﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	public int numb=0;
	public GameObject player;
	public GameObject tilePrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position.x > numb) {
						Instantiate (tilePrefab, new Vector2 (numb +10,-6), Quaternion.identity);
						numb += 1;
				}
	}
}
